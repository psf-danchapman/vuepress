---
title: Attachments
---

# Attachments
Currently as we don't have full control the uploads service doesn't work exactly like we need it to work, upon uploading a receipt we want to specify the user and also the claim description plus a date stamp. This is so people can refer to the physical files if needed.

We also need to be able to view/downloaded the uploaded file upon viewing the expense claim at either the input, authorisation or posting stage.