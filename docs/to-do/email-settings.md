---
title: Email Settings
---

# Email Settings
Have a page or tab that allows the admin to setup the email service, by defining things such as:
- Enable Emailing
- From address
- Host
- Port
- UseSSL
- Credentials: username and password