---
title: Dashboard
---

# Expense Dashboard

The Expense dashboard page is very simple, it contains three links:

- Link to expense input.
- Link to expense search that shows all statuses.
- Link to expense search with a predefined status filter of Created and Declined.

![alt text](https://bitbucket.org/psf-danchapman/vuepress/raw/master/docs/whats-done/images/expense-dashboard-links.png "Expense Dashboard")
