---
title: Input
---

# Expense Input

Expense input is split into three sections: the toolbar, claim header and claim details. 

To put it simply users are allowed to attach receipts taken from their phone or uploaded from any given location, they can then fill out the form based upon the information in the receipt and then submit it for authorisation.

![alt text](https://bitbucket.org/psf-danchapman/vuepress/raw/master/docs/whats-done/images/expense-input.PNG "Expense Input")

## Toolbar

![alt text](https://bitbucket.org/psf-danchapman/vuepress/raw/master/docs/whats-done/images/expense-input-toolbar.png "Expense Input Toolbar")

The toolbar is a separate component, it allows users to perform actions on their claim. The most important item we pass into the component is the selected claim because of that we can perform the required actions, this decluters the expense input controller which was a problem with previous PS Expenses. All of the logic for the toolbar is located within the expenseInputFactory this is because save and delete are required in other places, so we made it shareable instead of repeating ourselves.

## Claim Header

![alt text](https://bitbucket.org/psf-danchapman/vuepress/raw/master/docs/expenses/images/expense-input-header.png "Expense Input Header")

The Claim header is a separate component its simple and doesn't require any additional logic as it is always fixed.

## Claim Details

![alt text](https://bitbucket.org/psf-danchapman/vuepress/raw/master/docs/whats-done/images/expense-input-details.png "Expense Input Details")

The Claim details however require additional logic, upon clicking the split button in the toolbar you make the claim have multiple detail lines. So it changes and adds an additional field called amount, the amounts of all the detail lines must add up to the claim total amount.

The claim type field loads data from the claim type maintenance page, and based upon the selection it also uses the nominal/ledger/account combination that was predefined on the claim type. The selected ledger is used to filter the account picker and if the account is already defined then the account picker is pre-populated.

The Vat code field is currently loading keylistkeys from the 'VATRATES' keylist, and upon selecting one the details vat amount is calculated, either based on the claim amount (if only 1 line) or the details amount (if more than 1 line).

## Summary

Only when the form is valid and the claim balances (Only required when more than one detail line) are you allowed to save the claim.