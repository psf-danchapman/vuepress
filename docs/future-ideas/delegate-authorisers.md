---
title: Delegate Authorisers
---

# Delegate Authorisers
Similar to MyPortal but actually make it work, when authorisers are away on holiday (could be determined through timesheets), allow for an admin to define who will take over for the period that they're away.

Potentially we could notify the admin user, when a user submits something to an authoriser that is on holiday. Or the system could automatically attempt to send it to the delegate authoriser.