module.exports = {
    title: 'PS Expenses',
    description: 'PS Expenses documentation using VuePress.',
    themeConfig: {
        nav: [],
        sidebar: [
            {
                title: 'Whats Done',
                collapsable: false,
                children: [
                    '/whats-done/dashboard',
                    '/whats-done/input',
                    '/whats-done/search',
                    '/whats-done/authorisation',                    
                    '/whats-done/claimtypes',                    
                    '/whats-done/usersettings',                    
                    '/whats-done/config'                   
                ]
            },
            {
                title: 'To Do',
                collapsable: false,
                children: [
                    '/to-do/expense-posting',           
                    '/to-do/email-settings',
                    '/to-do/dafs',
                    '/to-do/attachments',
                    '/to-do/unit-testing',
                ]
            },
            {
                title: 'Future Ideas',
                collapsable: false,
                children: [
                    '/future-ideas/delegate-authorisers',
                    '/future-ideas/Independent-hematite',
                ]
            }
        ]
    }
};