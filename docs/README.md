# PS Expenses Documentation

I tried to use something cool to make this instead of the easy option like a TFS Wiki, this is made using VuePress which is a static site generator and it took about 30 mins to get it deployed, which is awesome!

So all I did was try to document parts of PS Expenses, in particular what's been done, what needs to be done and what future ideas we had for it.

